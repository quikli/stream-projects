import game
import importlib
import models
import os
import pygame
import time
import traceback


# TODO
# Game Design
# Maybe some sort of RPG
# Single Player, first person


def main():
    pygame.init()
    screen, clock, state, assets = game.initialize()

    reload = False
    running = True
    game_last_loaded = os.stat("game.py").st_mtime
    while running:

        # Check if game should be reloading because of an edit
        game_last_edited = os.stat("game.py").st_mtime
        if game_last_edited > game_last_loaded:
            reload = True

        while reload:
            # Until we successfully reload, keep trying to
            # reload once per second and printing out the errors
            try:
                print("reloading game")
                importlib.reload(game)
                game_last_loaded = game_last_edited
                reload = False
            except Exception as e:
                traceback.print_exception(e)
                time.sleep(1.0)

        try:
            # Run a frame of the game
            running = game.update_and_render(screen, clock, state, assets)

            # if the frame returned None, that's an error
            # go ahead and trigger us unto the hot reload loop
            assert running is not None

        except Exception as e:
            traceback.print_exception(e)
            time.sleep(1.0)
            reload = True

    pygame.quit()


if __name__ == "__main__":
    main()

