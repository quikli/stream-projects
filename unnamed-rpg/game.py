import random
import pygame
import models
from models import V2, Entity, Player, Controls

from typing import Dict, List


def initialize():
    screen = pygame.display.set_mode((1280, 720))
    clock = pygame.time.Clock()
    player = Player(
        image="player",
        health=100.0,
        position=V2(x=100.0, y=100.0),
    )
    assets = {}
    assets["player"] = pygame.image.load("assets/PNG/mapTile_136.png")
    assets["background"] = pygame.image.load("assets/PNG/mapTile_017.png")
    assets["cactus"] = pygame.image.load("assets/PNG/mapTile_035.png")
    assets["enemy"] = pygame.image.load("assets/PNG/mapTile_137.png")

    entities = [
        player,
        Entity(image="cactus", position=V2(x=50, y=100)),
        Entity(image="cactus", position=V2(x=300, y=350)),
        Entity(image="cactus", position=V2(x=450, y=172)),
        Entity(image="cactus", position=V2(x=300, y=217)),
        Entity(image="cactus", position=V2(x=600, y=50)),
        Entity(image="enemy", position=V2(x=600, y=50)),
    ]

    state = models.GameState(
        controls=models.Controls(w=False, a=False, s=False, d=False),
        player=player,
        entities=entities,
    )

    return screen, clock, state, assets


def update_and_render(
    screen: pygame.surface.Surface,
    clock: pygame.time.Clock,
    state: models.GameState,
    assets: Dict[str, pygame.surface.Surface],
) -> bool:
    """Updates & Renders the game
    Return is whether the game should keep running
    """

    player: Player = state.player
    controls: Controls = state.controls
    entities: List[Entity] = state.entities

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            return False
        elif event.type == pygame.KEYUP:
            if event.key == pygame.K_w:
                controls.w = False
            elif event.key == pygame.K_a:
                controls.a = False
            elif event.key == pygame.K_s:
                controls.s = False
            elif event.key == pygame.K_d:
                controls.d = False
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_w:
                controls.w = True
            elif event.key == pygame.K_a:
                controls.a = True
            elif event.key == pygame.K_s:
                controls.s = True
            elif event.key == pygame.K_d:
                controls.d = True

    enemies = [e for e in entities if e.image == "enemy"]

    for enemy in enemies:
        enemy.position = enemy.position + V2(
            x=random.random() - 0.5, y=random.random() - 0.5
        )
        if enemy.position.x < 0:
            enemy.position.x = 0
        if enemy.position.x > 800:
            enemy.position.x = 800
        if enemy.position.y < 0:
            enemy.position.y = 0
        if enemy.position.y > 800:
            enemy.position.y = 800

    # Player Velocity
    velocity = V2(x=0.0, y=0.0)
    if controls.w:
        velocity.y -= 1.0
    if controls.a:
        velocity.x -= 1.0
    if controls.s:
        velocity.y += 1.0
    if controls.d:
        velocity.x += 1.0

    l = velocity.length
    if l > 0:
        velocity = velocity / l
        velocity *= 4

    player.position = player.position + velocity

    collider_offset = V2(x=32, y=48)
    feet_position_player = player.position + collider_offset

    for entity in entities:
        if entity is not player:
            base_entity_position = entity.position + collider_offset
            vector_apart = base_entity_position - feet_position_player
            distance_apart = vector_apart.length
            if distance_apart < 30:
                player.position += vector_apart.normalized * (distance_apart - 30)
                player.health -= 1.0

    for x in range(15):
        for y in range(8):
            screen.blit(assets["background"], (x * 64, y * 64))

    entities.sort(key=lambda e: e.position.y)
    for entity in entities:
        screen.blit(assets[entity.image], entity.position.coordinates)

    if player.health < 100:
        screen.fill(
            "red", pygame.Rect(player.position.x + 8, player.position.y - 8, 48, 8)
        )
        screen.fill(
            "green",
            pygame.Rect(
                player.position.x + 8,
                player.position.y - 8,
                player.health * 48 / 100.0,
                8,
            ),
        )

    pygame.display.flip()

    clock.tick(60)  # limits FPS to 60

    return True
