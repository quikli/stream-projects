from dataclasses import dataclass
from typing import Tuple, List
import math


@dataclass
class Controls:
    w: bool
    a: bool
    s: bool
    d: bool


@dataclass
class V2:
    x: float
    y: float

    @property
    def coordinates(self):
        return self.x, self.y

    def __add__(self, other: 'V2'):
        return V2(x=self.x + other.x, y=self.y + other.y)

    def __sub__(self, other: 'V2'):
        return V2(x=self.x - other.x, y=self.y - other.y)

    def __mul__(self, other: float):
        return V2(x=self.x * other, y=self.y * other)

    def __div__(self, other: float):
        return V2(x=self.x / other, y=self.y / other)

    def __truediv__(self, other: float):
        return V2(x=self.x / other, y=self.y / other)

    def dot(self, other: 'V2'):
        return self.x * other.x + self.y * other.y

    @property
    def length(self):
        return math.sqrt(self.dot(self))

    @property
    def normalized(self):
        l = self.length
        if l == 0:
            return self
        return self / l


@dataclass
class Entity:
    image: str
    position: V2


@dataclass
class Player(Entity):
    health: float


@dataclass
class GameState:
    controls: Controls
    player: Player
    entities: List[Entity]

