# cython: language_level=3

from libc.stdint cimport uint8_t, uint16_t, uint32_t, int32_t


cdef extern from "SDL2/SDL.h":

    ctypedef void *SDL_GLContext

    ctypedef struct SDL_Joystick:
        pass

    ctypedef struct SDL_GameController:
        pass

    ctypedef struct SDL_RWops:
        # This is not opaque in SDL_rwops.h but I don't need to reach in
        pass

    struct SDL_Window:
        pass

    struct SDL_Renderer:
        pass

    struct SDL_BlitMap:
        pass

    struct SDL_Surface:
        pass

    struct SDL_Rect:
        int x, y, w, h

    struct SDL_Color:
        uint8_t r, g, b, a

    struct SDL_Texture:
        pass

    struct SDL_AudioDeviceEvent:
        uint32_t type
        uint32_t timestamp
        uint32_t which
        uint8_t iscapture

    struct SDL_ControllerButtonEvent:
        uint32_t type
        uint32_t timestamp
        int32_t which
        uint8_t button
        uint8_t state

    struct SDL_ControllerDeviceEvent:
        uint32_t type
        uint32_t timestamp
        int32_t which

    struct SDL_Keysym:
        int scancode
        int32_t sym
        uint16_t mod
        uint32_t unused

    struct SDL_KeyboardEvent:
        uint32_t type
        uint32_t timestamp
        uint32_t windowID
        uint8_t state
        uint8_t repeat
        SDL_Keysym keysym

    struct SDL_JoyHatEvent:
        uint32_t type
        uint32_t timestamp
        int which
        uint8_t hat
        uint8_t value

    struct SDL_MouseButtonEvent:
        uint32_t type
        uint32_t timestamp
        uint32_t windowID
        uint32_t which
        uint8_t button
        uint8_t state
        uint8_t clicks
        int32_t x
        int32_t y

    struct SDL_MouseMotionEvent:
        uint32_t type
        uint32_t timestamp
        uint32_t windowID
        uint32_t which
        uint32_t state
        int32_t x
        int32_t y
        int32_t xrel
        int32_t yrel

    struct SDL_MouseWheelEvent:
        uint32_t type
        uint32_t timestamp
        uint32_t windowID
        uint32_t which
        int32_t x
        int32_t y
        uint32_t direction

    struct SDL_QuitEvent:
        uint32_t type

    struct SDL_TextInputEvent:
        uint32_t type
        uint32_t timestamp
        uint32_t windowID
        char text[32]

    struct SDL_WindowEvent:
        uint32_t type
        uint32_t timestamp
        uint32_t windowID
        uint8_t event
        int32_t data1
        int32_t data2

    union SDL_Event:
        uint32_t type
        SDL_AudioDeviceEvent adevice
        SDL_ControllerButtonEvent cbutton
        SDL_ControllerDeviceEvent cdevice
        SDL_KeyboardEvent key
        SDL_JoyHatEvent jhat
        SDL_MouseButtonEvent button
        SDL_MouseMotionEvent motion
        SDL_MouseWheelEvent wheel
        SDL_QuitEvent quit
        SDL_TextInputEvent text
        SDL_WindowEvent window

    # Values
    int SDL_TEXTUREACCESS_TARGET

    uint32_t SDL_INIT_AUDIO
    uint32_t SDL_INIT_VIDEO
    uint32_t SDL_INIT_HAPTIC
    uint32_t SDL_INIT_GAMECONTROLLER
    uint32_t SDL_WINDOWPOS_UNDEFINED
    uint32_t SDL_WINDOW_SHOWN
    uint32_t SDL_WINDOW_OPENGL
    uint32_t SDL_WINDOW_RESIZABLE
    uint32_t SDL_RENDERER_ACCELERATED
    uint32_t SDL_RENDERER_TARGETTEXTURE
    uint32_t SDL_PIXELFORMAT_RGBA8888

    uint32_t SDL_AUDIODEVICEADDED
    uint32_t SDL_AUDIODEVICEREMOVED
    uint32_t SDL_CONTROLLERBUTTONDOWN
    uint32_t SDL_CONTROLLERBUTTONUP
    uint32_t SDL_CONTROLLERDEVICEADDED
    uint32_t SDL_CONTROLLERDEVICEREMOVED
    uint32_t SDL_JOYHATMOTION
    uint32_t SDL_KEYDOWN
    uint32_t SDL_KEYUP
    uint32_t SDL_MOUSEBUTTONDOWN
    uint32_t SDL_MOUSEBUTTONUP
    uint32_t SDL_MOUSEMOTION
    uint32_t SDL_MOUSEWHEEL
    uint32_t SDL_QUIT
    uint32_t SDL_TEXTINPUT
    uint32_t SDL_RENDER_TARGETS_RESET
    uint32_t SDL_WINDOWEVENT

    uint32_t SDL_WINDOWEVENT_SHOWN
    uint32_t SDL_WINDOWEVENT_HIDDEN
    uint32_t SDL_WINDOWEVENT_EXPOSED
    uint32_t SDL_WINDOWEVENT_MOVED
    uint32_t SDL_WINDOWEVENT_RESIZED
    uint32_t SDL_WINDOWEVENT_SIZE_CHANGED
    uint32_t SDL_WINDOWEVENT_MINIMIZED
    uint32_t SDL_WINDOWEVENT_MAXIMIZED
    uint32_t SDL_WINDOWEVENT_RESTORED
    uint32_t SDL_WINDOWEVENT_ENTER
    uint32_t SDL_WINDOWEVENT_LEAVE
    uint32_t SDL_WINDOWEVENT_FOCUS_GAINED
    uint32_t SDL_WINDOWEVENT_FOCUS_LOST
    uint32_t SDL_WINDOWEVENT_CLOSE

    uint8_t SDL_BUTTON_LEFT
    uint8_t SDL_BUTTON_MIDDLE
    uint8_t SDL_BUTTON_RIGHT
    uint8_t SDL_BUTTON_X1
    uint8_t SDL_BUTTON_X2
    uint8_t SDL_BUTTON_LMASK
    uint8_t SDL_BUTTON_MMASK
    uint8_t SDL_BUTTON_RMASK
    uint8_t SDL_BUTTON_X1MASK
    uint8_t SDL_BUTTON_X2MASK

    uint32_t SDL_MOUSEWHEEL_FLIPPED

    uint8_t SDL_PRESSED
    uint8_t SDL_RELEASED

    int32_t SDLK_UNKNOWN
    int32_t SDLK_RETURN
    int32_t SDLK_ESCAPE
    int32_t SDLK_BACKSPACE
    int32_t SDLK_TAB
    int32_t SDLK_SPACE
    int32_t SDLK_EXCLAIM
    int32_t SDLK_QUOTEDBL
    int32_t SDLK_HASH
    int32_t SDLK_PERCENT
    int32_t SDLK_DOLLAR
    int32_t SDLK_AMPERSAND
    int32_t SDLK_QUOTE
    int32_t SDLK_LEFTPAREN
    int32_t SDLK_RIGHTPAREN
    int32_t SDLK_ASTERISK
    int32_t SDLK_PLUS
    int32_t SDLK_COMMA
    int32_t SDLK_MINUS
    int32_t SDLK_PERIOD
    int32_t SDLK_SLASH
    int32_t SDLK_0
    int32_t SDLK_1
    int32_t SDLK_2
    int32_t SDLK_3
    int32_t SDLK_4
    int32_t SDLK_5
    int32_t SDLK_6
    int32_t SDLK_7
    int32_t SDLK_8
    int32_t SDLK_9
    int32_t SDLK_COLON
    int32_t SDLK_SEMICOLON
    int32_t SDLK_LESS
    int32_t SDLK_EQUALS
    int32_t SDLK_GREATER
    int32_t SDLK_QUESTION
    int32_t SDLK_AT
    int32_t SDLK_LEFTBRACKET
    int32_t SDLK_BACKSLASH
    int32_t SDLK_RIGHTBRACKET
    int32_t SDLK_CARET
    int32_t SDLK_UNDERSCORE
    int32_t SDLK_BACKQUOTE
    int32_t SDLK_a
    int32_t SDLK_b
    int32_t SDLK_c
    int32_t SDLK_d
    int32_t SDLK_e
    int32_t SDLK_f
    int32_t SDLK_g
    int32_t SDLK_h
    int32_t SDLK_i
    int32_t SDLK_j
    int32_t SDLK_k
    int32_t SDLK_l
    int32_t SDLK_m
    int32_t SDLK_n
    int32_t SDLK_o
    int32_t SDLK_p
    int32_t SDLK_q
    int32_t SDLK_r
    int32_t SDLK_s
    int32_t SDLK_t
    int32_t SDLK_u
    int32_t SDLK_v
    int32_t SDLK_w
    int32_t SDLK_x
    int32_t SDLK_y
    int32_t SDLK_z
    int32_t SDLK_CAPSLOCK
    int32_t SDLK_F1
    int32_t SDLK_F2
    int32_t SDLK_F3
    int32_t SDLK_F4
    int32_t SDLK_F5
    int32_t SDLK_F6
    int32_t SDLK_F7
    int32_t SDLK_F8
    int32_t SDLK_F9
    int32_t SDLK_F10
    int32_t SDLK_F11
    int32_t SDLK_F12
    int32_t SDLK_PRINTSCREEN
    int32_t SDLK_SCROLLLOCK
    int32_t SDLK_PAUSE
    int32_t SDLK_INSERT
    int32_t SDLK_HOME
    int32_t SDLK_PAGEUP
    int32_t SDLK_DELETE
    int32_t SDLK_END
    int32_t SDLK_PAGEDOWN
    int32_t SDLK_RIGHT
    int32_t SDLK_LEFT
    int32_t SDLK_DOWN
    int32_t SDLK_UP
    int32_t SDLK_NUMLOCKCLEAR
    int32_t SDLK_KP_DIVIDE
    int32_t SDLK_KP_MULTIPLY
    int32_t SDLK_KP_MINUS
    int32_t SDLK_KP_PLUS
    int32_t SDLK_KP_ENTER
    int32_t SDLK_KP_1
    int32_t SDLK_KP_2
    int32_t SDLK_KP_3
    int32_t SDLK_KP_4
    int32_t SDLK_KP_5
    int32_t SDLK_KP_6
    int32_t SDLK_KP_7
    int32_t SDLK_KP_8
    int32_t SDLK_KP_9
    int32_t SDLK_KP_0
    int32_t SDLK_KP_PERIOD
    int32_t SDLK_APPLICATION
    int32_t SDLK_POWER
    int32_t SDLK_KP_EQUALS
    int32_t SDLK_F13
    int32_t SDLK_F14
    int32_t SDLK_F15
    int32_t SDLK_F16
    int32_t SDLK_F17
    int32_t SDLK_F18
    int32_t SDLK_F19
    int32_t SDLK_F20
    int32_t SDLK_F21
    int32_t SDLK_F22
    int32_t SDLK_F23
    int32_t SDLK_F24
    int32_t SDLK_EXECUTE
    int32_t SDLK_HELP
    int32_t SDLK_MENU
    int32_t SDLK_SELECT
    int32_t SDLK_STOP
    int32_t SDLK_AGAIN
    int32_t SDLK_UNDO
    int32_t SDLK_CUT
    int32_t SDLK_COPY
    int32_t SDLK_PASTE
    int32_t SDLK_FIND
    int32_t SDLK_MUTE
    int32_t SDLK_VOLUMEUP
    int32_t SDLK_VOLUMEDOWN
    int32_t SDLK_KP_COMMA
    int32_t SDLK_KP_EQUALSAS400
    int32_t SDLK_ALTERASE
    int32_t SDLK_SYSREQ
    int32_t SDLK_CANCEL
    int32_t SDLK_CLEAR
    int32_t SDLK_PRIOR
    int32_t SDLK_RETURN2
    int32_t SDLK_SEPARATOR
    int32_t SDLK_OUT
    int32_t SDLK_OPER
    int32_t SDLK_CLEARAGAIN
    int32_t SDLK_CRSEL
    int32_t SDLK_EXSEL
    int32_t SDLK_KP_00
    int32_t SDLK_KP_000
    int32_t SDLK_THOUSANDSSEPARATOR
    int32_t SDLK_DECIMALSEPARATOR
    int32_t SDLK_CURRENCYUNIT
    int32_t SDLK_CURRENCYSUBUNIT
    int32_t SDLK_KP_LEFTPAREN
    int32_t SDLK_KP_RIGHTPAREN
    int32_t SDLK_KP_LEFTBRACE
    int32_t SDLK_KP_RIGHTBRACE
    int32_t SDLK_KP_TAB
    int32_t SDLK_KP_BACKSPACE
    int32_t SDLK_KP_A
    int32_t SDLK_KP_B
    int32_t SDLK_KP_C
    int32_t SDLK_KP_D
    int32_t SDLK_KP_E
    int32_t SDLK_KP_F
    int32_t SDLK_KP_XOR
    int32_t SDLK_KP_POWER
    int32_t SDLK_KP_PERCENT
    int32_t SDLK_KP_LESS
    int32_t SDLK_KP_GREATER
    int32_t SDLK_KP_AMPERSAND
    int32_t SDLK_KP_DBLAMPERSAND
    int32_t SDLK_KP_VERTICALBAR
    int32_t SDLK_KP_DBLVERTICALBAR
    int32_t SDLK_KP_COLON
    int32_t SDLK_KP_HASH
    int32_t SDLK_KP_SPACE
    int32_t SDLK_KP_AT
    int32_t SDLK_KP_EXCLAM
    int32_t SDLK_KP_MEMSTORE
    int32_t SDLK_KP_MEMRECALL
    int32_t SDLK_KP_MEMCLEAR
    int32_t SDLK_KP_MEMADD
    int32_t SDLK_KP_MEMSUBTRACT
    int32_t SDLK_KP_MEMMULTIPLY
    int32_t SDLK_KP_MEMDIVIDE
    int32_t SDLK_KP_PLUSMINUS
    int32_t SDLK_KP_CLEAR
    int32_t SDLK_KP_CLEARENTRY
    int32_t SDLK_KP_BINARY
    int32_t SDLK_KP_OCTAL
    int32_t SDLK_KP_DECIMAL
    int32_t SDLK_KP_HEXADECIMAL
    int32_t SDLK_LCTRL
    int32_t SDLK_LSHIFT
    int32_t SDLK_LALT
    int32_t SDLK_LGUI
    int32_t SDLK_RCTRL
    int32_t SDLK_RSHIFT
    int32_t SDLK_RALT
    int32_t SDLK_RGUI
    int32_t SDLK_MODE
    int32_t SDLK_AUDIONEXT
    int32_t SDLK_AUDIOPREV
    int32_t SDLK_AUDIOSTOP
    int32_t SDLK_AUDIOPLAY
    int32_t SDLK_AUDIOMUTE
    int32_t SDLK_MEDIASELECT
    int32_t SDLK_WWW
    int32_t SDLK_MAIL
    int32_t SDLK_CALCULATOR
    int32_t SDLK_COMPUTER
    int32_t SDLK_AC_SEARCH
    int32_t SDLK_AC_HOME
    int32_t SDLK_AC_BACK
    int32_t SDLK_AC_FORWARD
    int32_t SDLK_AC_STOP
    int32_t SDLK_AC_REFRESH
    int32_t SDLK_AC_BOOKMARKS
    int32_t SDLK_BRIGHTNESSDOWN
    int32_t SDLK_BRIGHTNESSUP
    int32_t SDLK_DISPLAYSWITCH
    int32_t SDLK_KBDILLUMTOGGLE
    int32_t SDLK_KBDILLUMDOWN
    int32_t SDLK_KBDILLUMUP
    int32_t SDLK_EJECT
    int32_t SDLK_SLEEP
    int32_t SDLK_APP1
    int32_t SDLK_APP2
    int32_t SDLK_AUDIOREWIND
    int32_t SDLK_AUDIOFASTFORWARD

    uint16_t KMOD_NONE
    uint16_t KMOD_LSHIFT
    uint16_t KMOD_RSHIFT
    uint16_t KMOD_LCTRL
    uint16_t KMOD_RCTRL
    uint16_t KMOD_LALT
    uint16_t KMOD_RALT
    uint16_t KMOD_LGUI
    uint16_t KMOD_RGUI
    uint16_t KMOD_NUM
    uint16_t KMOD_CAPS
    uint16_t KMOD_MODE
    uint16_t KMOD_RESERVED
    uint16_t KMOD_CTRL
    uint16_t KMOD_SHIFT
    uint16_t KMOD_ALT
    uint16_t KMOD_GUI

    uint8_t SDL_HAT_LEFTUP
    uint8_t SDL_HAT_UP
    uint8_t SDL_HAT_RIGHTUP
    uint8_t SDL_HAT_LEFT
    uint8_t SDL_HAT_CENTERED
    uint8_t SDL_HAT_RIGHT
    uint8_t SDL_HAT_LEFTDOWN
    uint8_t SDL_HAT_DOWN
    uint8_t SDL_HAT_RIGHTDOWN

    uint8_t SDL_CONTROLLER_BUTTON_INVALID
    uint8_t SDL_CONTROLLER_BUTTON_A
    uint8_t SDL_CONTROLLER_BUTTON_B
    uint8_t SDL_CONTROLLER_BUTTON_X
    uint8_t SDL_CONTROLLER_BUTTON_Y
    uint8_t SDL_CONTROLLER_BUTTON_BACK
    uint8_t SDL_CONTROLLER_BUTTON_GUIDE
    uint8_t SDL_CONTROLLER_BUTTON_START
    uint8_t SDL_CONTROLLER_BUTTON_LEFTSTICK
    uint8_t SDL_CONTROLLER_BUTTON_RIGHTSTICK
    uint8_t SDL_CONTROLLER_BUTTON_LEFTSHOULDER
    uint8_t SDL_CONTROLLER_BUTTON_RIGHTSHOULDER
    uint8_t SDL_CONTROLLER_BUTTON_DPAD_UP
    uint8_t SDL_CONTROLLER_BUTTON_DPAD_DOWN
    uint8_t SDL_CONTROLLER_BUTTON_DPAD_LEFT
    uint8_t SDL_CONTROLLER_BUTTON_DPAD_RIGHT

    # Functions
    int SDL_Init(uint32_t flags)
    void SDL_Quit()
    void SDL_Delay(uint32_t milliseconds)

    # Events
    int SDL_PollEvent(SDL_Event *event)

    # Errors
    const char *SDL_GetError()

    # RWOps
    SDL_RWops *SDL_RWFromFile(const char *file, const char *mode)

    # Joystick Functions
    SDL_GameController *SDL_GameControllerOpen(int joystick_index)
    SDL_Joystick *SDL_GameControllerGetJoystick(SDL_GameController *gamecontroller)
    int32_t SDL_JoystickInstanceID(SDL_Joystick *joystick)

    # Window functions
    SDL_Window *SDL_CreateWindow(const char* title, int x, int y, int w, int h, uint32_t flags)
    void SDL_DestroyWindow(SDL_Window *window)
    void SDL_GetWindowSize(SDL_Window *window, int *w, int *h)
    uint32_t SDL_GetWindowID(SDL_Window* window)

    # Renderer Fucntions
    SDL_Renderer *SDL_CreateRenderer(SDL_Window *window, int index, uint32_t flags)
    void SDL_DestroyRenderer(SDL_Renderer *renderer)
    int SDL_SetRenderDrawColor(SDL_Renderer *renderer, uint8_t r, uint8_t g, uint8_t b, uint8_t a)
    int SDL_RenderClear(SDL_Renderer *renderer)
    void SDL_RenderPresent(SDL_Renderer *renderer)
    int SDL_RenderCopy(SDL_Renderer *renderer, SDL_Texture *texture, const SDL_Rect *srcrect, const SDL_Rect *dstrect)
    int SDL_SetRenderTarget(SDL_Renderer *renderer, SDL_Texture *texture)

    # Texture Functions
    SDL_Texture *SDL_CreateTexture(SDL_Renderer *renderer, uint32_t format, int access, int w, int h)
    SDL_Texture *SDL_CreateTextureFromSurface(SDL_Renderer *renderer, SDL_Surface *surface)
    void SDL_DestroyTexture(SDL_Texture *texture)
    void SDL_FreeSurface(SDL_Surface *surface)
    int SDL_QueryTexture(SDL_Texture* texture, uint32_t* format, int* access, int* w, int* h)


    # Input state
    uint32_t SDL_GetMouseState(int *x, int *y)
    uint16_t SDL_GetModState()

    # OpenGL
    int SDL_GL_RED_SIZE
    int SDL_GL_GREEN_SIZE
    int SDL_GL_BLUE_SIZE
    int SDL_GL_ALPHA_SIZE
    int SDL_GL_BUFFER_SIZE
    int SDL_GL_DOUBLEBUFFER
    int SDL_GL_DEPTH_SIZE
    int SDL_GL_STENCIL_SIZE
    int SDL_GL_ACCUM_RED_SIZE
    int SDL_GL_ACCUM_GREEN_SIZE
    int SDL_GL_ACCUM_BLUE_SIZE
    int SDL_GL_ACCUM_ALPHA_SIZE
    int SDL_GL_STEREO
    int SDL_GL_MULTISAMPLEBUFFERS
    int SDL_GL_MULTISAMPLESAMPLES
    int SDL_GL_ACCELERATED_VISUAL
    int SDL_GL_RETAINED_BACKING
    int SDL_GL_CONTEXT_MAJOR_VERSION
    int SDL_GL_CONTEXT_MINOR_VERSION
    int SDL_GL_CONTEXT_EGL
    int SDL_GL_CONTEXT_FLAGS
    int SDL_GL_CONTEXT_PROFILE_MASK
    int SDL_GL_SHARE_WITH_CURRENT_CONTEXT
    int SDL_GL_FRAMEBUFFER_SRGB_CAPABLE
    int SDL_GL_CONTEXT_RELEASE_BEHAVIOR
    int SDL_GL_CONTEXT_RESET_NOTIFICATION
    int SDL_GL_CONTEXT_NO_ERROR
    int SDL_GL_FLOATBUFFER

    int SDL_GL_CONTEXT_PROFILE_CORE
    int SDL_GL_CONTEXT_PROFILE_COMPATIBILITY
    int SDL_GL_CONTEXT_PROFILE_ES

    int SDL_GL_SetAttribute(int attr, int value)

    SDL_GLContext SDL_GL_CreateContext(SDL_Window *window)
    void SDL_GL_DeleteContext(SDL_GLContext context)
    int SDL_GL_SetSwapInterval(int interval)


cdef extern from "SDL2/SDL_image.h":
    int IMG_INIT_JPG
    int IMG_INIT_PNG
    int IMG_INIT_TIF
    int IMG_INIT_WEBP

    int IMG_Init(int flags)
    void IMG_Quit()

    SDL_Texture *IMG_LoadTexture(SDL_Renderer *renderer, const char *file)


cdef extern from "SDL2/SDL_mixer.h":

    int MIX_INIT_FLAC
    int MIX_INIT_MOD
    int MIX_INIT_MODPLUG
    int MIX_INIT_MP3
    int MIX_INIT_OGG
    int MIX_INIT_FLUIDSYNTH

    ctypedef struct Mix_Chunk:
        int allocated
        uint8_t *abuf
        uint32_t alen
        uint8_t volume

    uint8_t MIX_NO_FADING
    uint8_t MIX_FADING_OUT
    uint8_t MIX_FADING_IN

    uint8_t MUS_NONE
    uint8_t MUS_CMD
    uint8_t MUS_WAV
    uint8_t MUS_MOD
    uint8_t MUS_MID
    uint8_t MUS_MP3
    uint8_t MUS_MP3_MAD_UNUSED
    uint8_t MUS_FLAC
    uint8_t MUS_MODPLUG_UNUSED

    int MIX_CHANNELS
    int MIX_DEFAULT_FREQUENCY
    uint16_t MIX_DEFAULT_FORMAT
    int MIX_DEFAULT_CHANNELS
    uint8_t MIX_MAX_VOLUME

    ctypedef struct Mix_Music:
        pass

    int Mix_Init(int flags)
    void Mix_Quit()
    int Mix_OpenAudio(int frequency, uint16_t format, int channels, int chunksize)
    int Mix_OpenAudioDevice(
        int frequency,
        uint16_t format,
        int channels,
        int chunksize,
        const char* device,
        int allowed_changes
    )
    int Mix_AllocateChannels(int numchans)
    int Mix_QuerySpec(int *frequency, uint16_t *format, int *channels)
    Mix_Music *Mix_LoadMUS(const char *file)
    Mix_Chunk *Mix_LoadWAV_RW(SDL_RWops *src, int freesrc)
    void Mix_FreeChunk(Mix_Chunk *chunk)
    void Mix_FreeMusic(Mix_Music *music)
    int Mix_PlayChannelTimed(int channel, Mix_Chunk *chunk, int loops, int ticks)
    int Mix_PlayMusic(Mix_Music *music, int loops)
    int Mix_VolumeChunk(Mix_Chunk *chunk, int volume)
    int Mix_VolumeMusic(int volume)
    int Mix_HaltMusic()
    void Mix_PauseMusic()
    void Mix_ResumeMusic()
    void Mix_RewindMusic()
    int Mix_PausedMusic()
    int Mix_PlayingMusic()
    void Mix_CloseAudio()


cdef extern from "GL/glew.h":

    ctypedef int GLint
    ctypedef unsigned int GLuint
    ctypedef unsigned char GLboolean
    ctypedef unsigned int GLenum
    ctypedef char GLchar
    ctypedef int GLsizei

    int GL_TRUE
    int GL_FALSE
    int GL_ZERO
    int GL_LOGIC_OP
    int GL_NONE
    int GL_TEXTURE_COMPONENTS
    int GL_NO_ERROR
    int GL_POINTS
    int GL_CURRENT_BIT
    int GL_ONE
    int GL_CLIENT_PIXEL_STORE_BIT
    int GL_LINES
    int GL_LINE_LOOP
    int GL_POINT_BIT
    int GL_CLIENT_VERTEX_ARRAY_BIT
    int GL_LINE_STRIP
    int GL_LINE_BIT
    int GL_TRIANGLES
    int GL_TRIANGLE_STRIP
    int GL_TRIANGLE_FAN
    int GL_QUADS
    int GL_QUAD_STRIP
    int GL_POLYGON_BIT
    int GL_POLYGON
    int GL_POLYGON_STIPPLE_BIT
    int GL_PIXEL_MODE_BIT
    int GL_LIGHTING_BIT
    int GL_FOG_BIT
    int GL_DEPTH_BUFFER_BIT
    int GL_ACCUM
    int GL_LOAD
    int GL_RETURN
    int GL_MULT
    int GL_ADD
    int GL_NEVER
    int GL_ACCUM_BUFFER_BIT
    int GL_LESS
    int GL_EQUAL
    int GL_LEQUAL
    int GL_GREATER
    int GL_NOTEQUAL
    int GL_GEQUAL
    int GL_ALWAYS
    int GL_SRC_COLOR
    int GL_ONE_MINUS_SRC_COLOR
    int GL_SRC_ALPHA
    int GL_ONE_MINUS_SRC_ALPHA
    int GL_DST_ALPHA
    int GL_ONE_MINUS_DST_ALPHA
    int GL_DST_COLOR
    int GL_ONE_MINUS_DST_COLOR
    int GL_SRC_ALPHA_SATURATE
    int GL_STENCIL_BUFFER_BIT
    int GL_FRONT_LEFT
    int GL_FRONT_RIGHT
    int GL_BACK_LEFT
    int GL_BACK_RIGHT
    int GL_FRONT
    int GL_BACK
    int GL_LEFT
    int GL_RIGHT
    int GL_FRONT_AND_BACK
    int GL_AUX0
    int GL_AUX1
    int GL_AUX2
    int GL_AUX3
    int GL_INVALID_ENUM
    int GL_INVALID_VALUE
    int GL_INVALID_OPERATION
    int GL_STACK_OVERFLOW
    int GL_STACK_UNDERFLOW
    int GL_OUT_OF_MEMORY
    int GL_2D
    int GL_3D
    int GL_3D_COLOR
    int GL_3D_COLOR_TEXTURE
    int GL_4D_COLOR_TEXTURE
    int GL_PASS_THROUGH_TOKEN
    int GL_POINT_TOKEN
    int GL_LINE_TOKEN
    int GL_POLYGON_TOKEN
    int GL_BITMAP_TOKEN
    int GL_DRAW_PIXEL_TOKEN
    int GL_COPY_PIXEL_TOKEN
    int GL_LINE_RESET_TOKEN
    int GL_EXP
    int GL_VIEWPORT_BIT
    int GL_EXP2
    int GL_CW
    int GL_CCW
    int GL_COEFF
    int GL_ORDER
    int GL_DOMAIN
    int GL_CURRENT_COLOR
    int GL_CURRENT_INDEX
    int GL_CURRENT_NORMAL
    int GL_CURRENT_TEXTURE_COORDS
    int GL_CURRENT_RASTER_COLOR
    int GL_CURRENT_RASTER_INDEX
    int GL_CURRENT_RASTER_TEXTURE_COORDS
    int GL_CURRENT_RASTER_POSITION
    int GL_CURRENT_RASTER_POSITION_VALID
    int GL_CURRENT_RASTER_DISTANCE
    int GL_POINT_SMOOTH
    int GL_POINT_SIZE
    int GL_POINT_SIZE_RANGE
    int GL_POINT_SIZE_GRANULARITY
    int GL_LINE_SMOOTH
    int GL_LINE_WIDTH
    int GL_LINE_WIDTH_RANGE
    int GL_LINE_WIDTH_GRANULARITY
    int GL_LINE_STIPPLE
    int GL_LINE_STIPPLE_PATTERN
    int GL_LINE_STIPPLE_REPEAT
    int GL_LIST_MODE
    int GL_MAX_LIST_NESTING
    int GL_LIST_BASE
    int GL_LIST_INDEX
    int GL_POLYGON_MODE
    int GL_POLYGON_SMOOTH
    int GL_POLYGON_STIPPLE
    int GL_EDGE_FLAG
    int GL_CULL_FACE
    int GL_CULL_FACE_MODE
    int GL_FRONT_FACE
    int GL_LIGHTING
    int GL_LIGHT_MODEL_LOCAL_VIEWER
    int GL_LIGHT_MODEL_TWO_SIDE
    int GL_LIGHT_MODEL_AMBIENT
    int GL_SHADE_MODEL
    int GL_COLOR_MATERIAL_FACE
    int GL_COLOR_MATERIAL_PARAMETER
    int GL_COLOR_MATERIAL
    int GL_FOG
    int GL_FOG_INDEX
    int GL_FOG_DENSITY
    int GL_FOG_START
    int GL_FOG_END
    int GL_FOG_MODE
    int GL_FOG_COLOR
    int GL_DEPTH_RANGE
    int GL_DEPTH_TEST
    int GL_DEPTH_WRITEMASK
    int GL_DEPTH_CLEAR_VALUE
    int GL_DEPTH_FUNC
    int GL_ACCUM_CLEAR_VALUE
    int GL_STENCIL_TEST
    int GL_STENCIL_CLEAR_VALUE
    int GL_STENCIL_FUNC
    int GL_STENCIL_VALUE_MASK
    int GL_STENCIL_FAIL
    int GL_STENCIL_PASS_DEPTH_FAIL
    int GL_STENCIL_PASS_DEPTH_PASS
    int GL_STENCIL_REF
    int GL_STENCIL_WRITEMASK
    int GL_MATRIX_MODE
    int GL_NORMALIZE
    int GL_VIEWPORT
    int GL_MODELVIEW_STACK_DEPTH
    int GL_PROJECTION_STACK_DEPTH
    int GL_TEXTURE_STACK_DEPTH
    int GL_MODELVIEW_MATRIX
    int GL_PROJECTION_MATRIX
    int GL_TEXTURE_MATRIX
    int GL_ATTRIB_STACK_DEPTH
    int GL_CLIENT_ATTRIB_STACK_DEPTH
    int GL_ALPHA_TEST
    int GL_ALPHA_TEST_FUNC
    int GL_ALPHA_TEST_REF
    int GL_DITHER
    int GL_BLEND_DST
    int GL_BLEND_SRC
    int GL_BLEND
    int GL_LOGIC_OP_MODE
    int GL_INDEX_LOGIC_OP
    int GL_COLOR_LOGIC_OP
    int GL_AUX_BUFFERS
    int GL_DRAW_BUFFER
    int GL_READ_BUFFER
    int GL_SCISSOR_BOX
    int GL_SCISSOR_TEST
    int GL_INDEX_CLEAR_VALUE
    int GL_INDEX_WRITEMASK
    int GL_COLOR_CLEAR_VALUE
    int GL_COLOR_WRITEMASK
    int GL_INDEX_MODE
    int GL_RGBA_MODE
    int GL_DOUBLEBUFFER
    int GL_STEREO
    int GL_RENDER_MODE
    int GL_PERSPECTIVE_CORRECTION_HINT
    int GL_POINT_SMOOTH_HINT
    int GL_LINE_SMOOTH_HINT
    int GL_POLYGON_SMOOTH_HINT
    int GL_FOG_HINT
    int GL_TEXTURE_GEN_S
    int GL_TEXTURE_GEN_T
    int GL_TEXTURE_GEN_R
    int GL_TEXTURE_GEN_Q
    int GL_PIXEL_MAP_I_TO_I
    int GL_PIXEL_MAP_S_TO_S
    int GL_PIXEL_MAP_I_TO_R
    int GL_PIXEL_MAP_I_TO_G
    int GL_PIXEL_MAP_I_TO_B
    int GL_PIXEL_MAP_I_TO_A
    int GL_PIXEL_MAP_R_TO_R
    int GL_PIXEL_MAP_G_TO_G
    int GL_PIXEL_MAP_B_TO_B
    int GL_PIXEL_MAP_A_TO_A
    int GL_PIXEL_MAP_I_TO_I_SIZE
    int GL_PIXEL_MAP_S_TO_S_SIZE
    int GL_PIXEL_MAP_I_TO_R_SIZE
    int GL_PIXEL_MAP_I_TO_G_SIZE
    int GL_PIXEL_MAP_I_TO_B_SIZE
    int GL_PIXEL_MAP_I_TO_A_SIZE
    int GL_PIXEL_MAP_R_TO_R_SIZE
    int GL_PIXEL_MAP_G_TO_G_SIZE
    int GL_PIXEL_MAP_B_TO_B_SIZE
    int GL_PIXEL_MAP_A_TO_A_SIZE
    int GL_UNPACK_SWAP_BYTES
    int GL_UNPACK_LSB_FIRST
    int GL_UNPACK_ROW_LENGTH
    int GL_UNPACK_SKIP_ROWS
    int GL_UNPACK_SKIP_PIXELS
    int GL_UNPACK_ALIGNMENT
    int GL_PACK_SWAP_BYTES
    int GL_PACK_LSB_FIRST
    int GL_PACK_ROW_LENGTH
    int GL_PACK_SKIP_ROWS
    int GL_PACK_SKIP_PIXELS
    int GL_PACK_ALIGNMENT
    int GL_MAP_COLOR
    int GL_MAP_STENCIL
    int GL_INDEX_SHIFT
    int GL_INDEX_OFFSET
    int GL_RED_SCALE
    int GL_RED_BIAS
    int GL_ZOOM_X
    int GL_ZOOM_Y
    int GL_GREEN_SCALE
    int GL_GREEN_BIAS
    int GL_BLUE_SCALE
    int GL_BLUE_BIAS
    int GL_ALPHA_SCALE
    int GL_ALPHA_BIAS
    int GL_DEPTH_SCALE
    int GL_DEPTH_BIAS
    int GL_MAX_EVAL_ORDER
    int GL_MAX_LIGHTS
    int GL_MAX_CLIP_PLANES
    int GL_MAX_TEXTURE_SIZE
    int GL_MAX_PIXEL_MAP_TABLE
    int GL_MAX_ATTRIB_STACK_DEPTH
    int GL_MAX_MODELVIEW_STACK_DEPTH
    int GL_MAX_NAME_STACK_DEPTH
    int GL_MAX_PROJECTION_STACK_DEPTH
    int GL_MAX_TEXTURE_STACK_DEPTH
    int GL_MAX_VIEWPORT_DIMS
    int GL_MAX_CLIENT_ATTRIB_STACK_DEPTH
    int GL_SUBPIXEL_BITS
    int GL_INDEX_BITS
    int GL_RED_BITS
    int GL_GREEN_BITS
    int GL_BLUE_BITS
    int GL_ALPHA_BITS
    int GL_DEPTH_BITS
    int GL_STENCIL_BITS
    int GL_ACCUM_RED_BITS
    int GL_ACCUM_GREEN_BITS
    int GL_ACCUM_BLUE_BITS
    int GL_ACCUM_ALPHA_BITS
    int GL_NAME_STACK_DEPTH
    int GL_AUTO_NORMAL
    int GL_MAP1_COLOR_4
    int GL_MAP1_INDEX
    int GL_MAP1_NORMAL
    int GL_MAP1_TEXTURE_COORD_1
    int GL_MAP1_TEXTURE_COORD_2
    int GL_MAP1_TEXTURE_COORD_3
    int GL_MAP1_TEXTURE_COORD_4
    int GL_MAP1_VERTEX_3
    int GL_MAP1_VERTEX_4
    int GL_MAP2_COLOR_4
    int GL_MAP2_INDEX
    int GL_MAP2_NORMAL
    int GL_MAP2_TEXTURE_COORD_1
    int GL_MAP2_TEXTURE_COORD_2
    int GL_MAP2_TEXTURE_COORD_3
    int GL_MAP2_TEXTURE_COORD_4
    int GL_MAP2_VERTEX_3
    int GL_MAP2_VERTEX_4
    int GL_MAP1_GRID_DOMAIN
    int GL_MAP1_GRID_SEGMENTS
    int GL_MAP2_GRID_DOMAIN
    int GL_MAP2_GRID_SEGMENTS
    int GL_TEXTURE_1D
    int GL_TEXTURE_2D
    int GL_FEEDBACK_BUFFER_POINTER
    int GL_FEEDBACK_BUFFER_SIZE
    int GL_FEEDBACK_BUFFER_TYPE
    int GL_SELECTION_BUFFER_POINTER
    int GL_SELECTION_BUFFER_SIZE
    int GL_TEXTURE_WIDTH
    int GL_TRANSFORM_BIT
    int GL_TEXTURE_HEIGHT
    int GL_TEXTURE_INTERNAL_FORMAT
    int GL_TEXTURE_BORDER_COLOR
    int GL_TEXTURE_BORDER
    int GL_DONT_CARE
    int GL_FASTEST
    int GL_NICEST
    int GL_AMBIENT
    int GL_DIFFUSE
    int GL_SPECULAR
    int GL_POSITION
    int GL_SPOT_DIRECTION
    int GL_SPOT_EXPONENT
    int GL_SPOT_CUTOFF
    int GL_CONSTANT_ATTENUATION
    int GL_LINEAR_ATTENUATION
    int GL_QUADRATIC_ATTENUATION
    int GL_COMPILE
    int GL_COMPILE_AND_EXECUTE
    int GL_BYTE
    int GL_UNSIGNED_BYTE
    int GL_SHORT
    int GL_UNSIGNED_SHORT
    int GL_INT
    int GL_UNSIGNED_INT
    int GL_FLOAT
    int GL_2_BYTES
    int GL_3_BYTES
    int GL_4_BYTES
    int GL_DOUBLE
    int GL_CLEAR
    int GL_AND
    int GL_AND_REVERSE
    int GL_COPY
    int GL_AND_INVERTED
    int GL_NOOP
    int GL_XOR
    int GL_OR
    int GL_NOR
    int GL_EQUIV
    int GL_INVERT
    int GL_OR_REVERSE
    int GL_COPY_INVERTED
    int GL_OR_INVERTED
    int GL_NAND
    int GL_SET
    int GL_EMISSION
    int GL_SHININESS
    int GL_AMBIENT_AND_DIFFUSE
    int GL_COLOR_INDEXES
    int GL_MODELVIEW
    int GL_PROJECTION
    int GL_TEXTURE
    int GL_COLOR
    int GL_DEPTH
    int GL_STENCIL
    int GL_COLOR_INDEX
    int GL_STENCIL_INDEX
    int GL_DEPTH_COMPONENT
    int GL_RED
    int GL_GREEN
    int GL_BLUE
    int GL_ALPHA
    int GL_RGB
    int GL_RGBA
    int GL_LUMINANCE
    int GL_LUMINANCE_ALPHA
    int GL_BITMAP
    int GL_POINT
    int GL_LINE
    int GL_FILL
    int GL_RENDER
    int GL_FEEDBACK
    int GL_SELECT
    int GL_FLAT
    int GL_SMOOTH
    int GL_KEEP
    int GL_REPLACE
    int GL_INCR
    int GL_DECR
    int GL_VENDOR
    int GL_RENDERER
    int GL_VERSION
    int GL_EXTENSIONS
    int GL_S
    int GL_ENABLE_BIT
    int GL_T
    int GL_R
    int GL_Q
    int GL_MODULATE
    int GL_DECAL
    int GL_TEXTURE_ENV_MODE
    int GL_TEXTURE_ENV_COLOR
    int GL_TEXTURE_ENV
    int GL_EYE_LINEAR
    int GL_OBJECT_LINEAR
    int GL_SPHERE_MAP
    int GL_TEXTURE_GEN_MODE
    int GL_OBJECT_PLANE
    int GL_EYE_PLANE
    int GL_NEAREST
    int GL_LINEAR
    int GL_NEAREST_MIPMAP_NEAREST
    int GL_LINEAR_MIPMAP_NEAREST
    int GL_NEAREST_MIPMAP_LINEAR
    int GL_LINEAR_MIPMAP_LINEAR
    int GL_TEXTURE_MAG_FILTER
    int GL_TEXTURE_MIN_FILTER
    int GL_TEXTURE_WRAP_S
    int GL_TEXTURE_WRAP_T
    int GL_CLAMP
    int GL_REPEAT
    int GL_POLYGON_OFFSET_UNITS
    int GL_POLYGON_OFFSET_POINT
    int GL_POLYGON_OFFSET_LINE
    int GL_R3_G3_B2
    int GL_V2F
    int GL_V3F
    int GL_C4UB_V2F
    int GL_C4UB_V3F
    int GL_C3F_V3F
    int GL_N3F_V3F
    int GL_C4F_N3F_V3F
    int GL_T2F_V3F
    int GL_T4F_V4F
    int GL_T2F_C4UB_V3F
    int GL_T2F_C3F_V3F
    int GL_T2F_N3F_V3F
    int GL_T2F_C4F_N3F_V3F
    int GL_T4F_C4F_N3F_V4F
    int GL_CLIP_PLANE0
    int GL_CLIP_PLANE1
    int GL_CLIP_PLANE2
    int GL_CLIP_PLANE3
    int GL_CLIP_PLANE4
    int GL_CLIP_PLANE5
    int GL_LIGHT0
    int GL_COLOR_BUFFER_BIT
    int GL_LIGHT1
    int GL_LIGHT2
    int GL_LIGHT3
    int GL_LIGHT4
    int GL_LIGHT5
    int GL_LIGHT6
    int GL_LIGHT7
    int GL_HINT_BIT
    int GL_POLYGON_OFFSET_FILL
    int GL_POLYGON_OFFSET_FACTOR
    int GL_ALPHA4
    int GL_ALPHA8
    int GL_ALPHA12
    int GL_ALPHA16
    int GL_LUMINANCE4
    int GL_LUMINANCE8
    int GL_LUMINANCE12
    int GL_LUMINANCE16
    int GL_LUMINANCE4_ALPHA4
    int GL_LUMINANCE6_ALPHA2
    int GL_LUMINANCE8_ALPHA8
    int GL_LUMINANCE12_ALPHA4
    int GL_LUMINANCE12_ALPHA12
    int GL_LUMINANCE16_ALPHA16
    int GL_INTENSITY
    int GL_INTENSITY4
    int GL_INTENSITY8
    int GL_INTENSITY12
    int GL_INTENSITY16
    int GL_RGB4
    int GL_RGB5
    int GL_RGB8
    int GL_RGB10
    int GL_RGB12
    int GL_RGB16
    int GL_RGBA2
    int GL_RGBA4
    int GL_RGB5_A1
    int GL_RGBA8
    int GL_RGB10_A2
    int GL_RGBA12
    int GL_RGBA16
    int GL_TEXTURE_RED_SIZE
    int GL_TEXTURE_GREEN_SIZE
    int GL_TEXTURE_BLUE_SIZE
    int GL_TEXTURE_ALPHA_SIZE
    int GL_TEXTURE_LUMINANCE_SIZE
    int GL_TEXTURE_INTENSITY_SIZE
    int GL_PROXY_TEXTURE_1D
    int GL_PROXY_TEXTURE_2D
    int GL_TEXTURE_PRIORITY
    int GL_TEXTURE_RESIDENT
    int GL_TEXTURE_BINDING_1D
    int GL_TEXTURE_BINDING_2D
    int GL_VERTEX_ARRAY
    int GL_NORMAL_ARRAY
    int GL_COLOR_ARRAY
    int GL_INDEX_ARRAY
    int GL_TEXTURE_COORD_ARRAY
    int GL_EDGE_FLAG_ARRAY
    int GL_VERTEX_ARRAY_SIZE
    int GL_VERTEX_ARRAY_TYPE
    int GL_VERTEX_ARRAY_STRIDE
    int GL_NORMAL_ARRAY_TYPE
    int GL_NORMAL_ARRAY_STRIDE
    int GL_COLOR_ARRAY_SIZE
    int GL_COLOR_ARRAY_TYPE
    int GL_COLOR_ARRAY_STRIDE
    int GL_INDEX_ARRAY_TYPE
    int GL_INDEX_ARRAY_STRIDE
    int GL_TEXTURE_COORD_ARRAY_SIZE
    int GL_TEXTURE_COORD_ARRAY_TYPE
    int GL_TEXTURE_COORD_ARRAY_STRIDE
    int GL_EDGE_FLAG_ARRAY_STRIDE
    int GL_VERTEX_ARRAY_POINTER
    int GL_NORMAL_ARRAY_POINTER
    int GL_COLOR_ARRAY_POINTER
    int GL_INDEX_ARRAY_POINTER
    int GL_TEXTURE_COORD_ARRAY_POINTER
    int GL_EDGE_FLAG_ARRAY_POINTER
    int GL_COLOR_INDEX1_EXT
    int GL_COLOR_INDEX2_EXT
    int GL_COLOR_INDEX4_EXT
    int GL_COLOR_INDEX8_EXT
    int GL_COLOR_INDEX12_EXT
    int GL_COLOR_INDEX16_EXT
    int GL_EVAL_BIT
    int GL_LIST_BIT
    int GL_TEXTURE_BIT
    int GL_SCISSOR_BIT
    int GL_ALL_ATTRIB_BITS
    int GL_CLIENT_ALL_ATTRIB_BITS

    int GLEW_OK = 0
    int GLEW_NO_ERROR = 0
    int GLEW_ERROR_NO_GL_VERSION = 1
    int GLEW_ERROR_GL_VERSION_10_ONLY = 2
    int GLEW_ERROR_GLX_VERSION_11_ONLY = 3
    int GLEW_ERROR_NO_GLX_DISPLAY = 4

    int GL_VERTEX_SHADER = 0x8B31
    int GL_COMPILE_STATUS = 0x8B81

    GLboolean glewExperimental
    GLenum glewInit()
    GLuint glCreateProgram()
    GLuint glCreateShader(GLenum kind)
    void glShaderSource(
        GLuint shader,
        GLsizei count,
        const GLchar *const*string,
        const GLint *length
    )
    void glCompileShader(GLuint shader)
    void glGetShaderiv(GLuint shader, GLenum pname, GLint *params)


cdef extern from "GL/glu.h":
    pass


cdef class App:

    cdef int window_open
    cdef SDL_Window *window
    cdef SDL_GLContext context

    def __cinit__(self):
        self.window_open = 0
        self.window = NULL
        self.context = NULL
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3)
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 1)
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE)

    def open_window(self, str title, int width, int height):
        assert self.window_open == 0

        # Create a Window
        self.window = SDL_CreateWindow(
            title.encode("utf-8"),
            SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
            width, height,
            SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE,
        )
        assert self.window != NULL

        # Create an OpenGL Context
        self.context = SDL_GL_CreateContext(self.window)
        assert self.context != NULL

        # Initialize GLEW
        glewExperimental = GL_TRUE
        cdef GLenum glew_error = glewInit()
        assert glew_error == GLEW_OK

        # Turn on Vsync
        assert SDL_GL_SetSwapInterval(1) >= 0

        cdef GLuint program_id = glCreateProgram()
        cdef GLuint vertex_shader = glCreateShader(GL_VERTEX_SHADER)

        cdef const GLchar *vertex_shader_source[1]
        vertex_shader_source = [
            """
                #version 140

                in vec2 LVertexPos2D;
                void main() {
                    gl_Position = vec4(LVertexPos2D.x, LVertexPos2D.y, 0, 1);
                }
            """.encode("utf-8")
        ]

        glShaderSource(vertex_shader, 1, vertex_shader_source, NULL)
        glCompileShader(vertex_shader)

        cdef GLint vertex_shader_compiled = GL_FALSE
        glGetShaderiv(vertex_shader, GL_COMPILE_STATUS, &vertex_shader_compiled)
        assert vertex_shader_compiled == GL_TRUE

        self.window_open = 1

    def close_window(self):
        assert self.window_open == 1
        SDL_GL_DeleteContext(self.context)
        SDL_DestroyWindow(self.window)
        self.window = NULL
        self.context = NULL

