from setuptools import Extension, setup # type: ignore
from Cython.Build import cythonize # type: ignore

extensions = [
    Extension(
        "plat",
        sources=["plat.pyx"],
        libraries=["SDL2", "GL", "GLU", "GLEW"]
    )
]

setup(ext_modules=cythonize(extensions))

