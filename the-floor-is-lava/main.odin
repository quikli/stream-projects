package main

import "core:fmt"
import "core:math/linalg"
import "vendor:sdl2"
import "vendor:sdl2/image"


ImageTag :: enum {
    IMAGES_COIN_0,
    IMAGES_COIN_1,
    IMAGES_COIN_2,
    IMAGES_COIN_3,
    IMAGES_COIN_4,
    IMAGES_COIN_5,
    IMAGES_FLOOR,
    IMAGES_FOUNDATION,
    IMAGES_LAVA,
    IMAGES_PIT0001,
    IMAGES_PIT0010,
    IMAGES_PIT0011,
    IMAGES_PIT0100,
    IMAGES_PIT0101,
    IMAGES_PIT0110,
    IMAGES_PIT0111,
    IMAGES_PIT1000,
    IMAGES_PIT1001,
    IMAGES_PIT1010,
    IMAGES_PIT1011,
    IMAGES_PIT1100,
    IMAGES_PIT1101,
    IMAGES_PIT1110,
    IMAGES_PIT1111,
    IMAGES_PLAYER_0,
    IMAGES_PLAYER_1,
    IMAGES_PLAYER_2,
    IMAGES_PLAYER_3,
    IMAGES_PLAYER_4,
    IMAGES_SPLASH,
    COUNT_IMAGES,
}


IMAGE_PATHS := [?]cstring{
    "images/coin_0.png",
    "images/coin_1.png",
    "images/coin_2.png",
    "images/coin_3.png",
    "images/coin_4.png",
    "images/coin_5.png",
    "images/floor.png",
    "images/foundation.png",
    "images/lava.png",
    "images/pit0001.png",
    "images/pit0010.png",
    "images/pit0011.png",
    "images/pit0100.png",
    "images/pit0101.png",
    "images/pit0110.png",
    "images/pit0111.png",
    "images/pit1000.png",
    "images/pit1001.png",
    "images/pit1010.png",
    "images/pit1011.png",
    "images/pit1100.png",
    "images/pit1101.png",
    "images/pit1110.png",
    "images/pit1111.png",
    "images/player_0.png",
    "images/player_1.png",
    "images/player_2.png",
    "images/player_3.png",
    "images/player_4.png",
    "images/splash.png",
}


COINCYCLE := [?]ImageTag{
    ImageTag.IMAGES_COIN_0,
    ImageTag.IMAGES_COIN_0,
    ImageTag.IMAGES_COIN_0,
    ImageTag.IMAGES_COIN_0,
    ImageTag.IMAGES_COIN_1,
    ImageTag.IMAGES_COIN_1,
    ImageTag.IMAGES_COIN_1,
    ImageTag.IMAGES_COIN_2,
    ImageTag.IMAGES_COIN_2,
    ImageTag.IMAGES_COIN_3,
    ImageTag.IMAGES_COIN_3,
    ImageTag.IMAGES_COIN_3,
    ImageTag.IMAGES_COIN_4,
    ImageTag.IMAGES_COIN_4,
    ImageTag.IMAGES_COIN_5,
    ImageTag.IMAGES_COIN_5,
    ImageTag.IMAGES_COIN_5,
}


RUNCYCLE := [?]ImageTag{
    ImageTag.IMAGES_PLAYER_2,
    ImageTag.IMAGES_PLAYER_2,
    ImageTag.IMAGES_PLAYER_2,
    ImageTag.IMAGES_PLAYER_2,
    ImageTag.IMAGES_PLAYER_3,
    ImageTag.IMAGES_PLAYER_3,
    ImageTag.IMAGES_PLAYER_3,
    ImageTag.IMAGES_PLAYER_3,
    ImageTag.IMAGES_PLAYER_4,
    ImageTag.IMAGES_PLAYER_4,
    ImageTag.IMAGES_PLAYER_4,
    ImageTag.IMAGES_PLAYER_4,
    ImageTag.IMAGES_PLAYER_3,
    ImageTag.IMAGES_PLAYER_3,
    ImageTag.IMAGES_PLAYER_3,
    ImageTag.IMAGES_PLAYER_3,
    ImageTag.IMAGES_PLAYER_2,
    ImageTag.IMAGES_PLAYER_2,
    ImageTag.IMAGES_PLAYER_2,
    ImageTag.IMAGES_PLAYER_2,
    ImageTag.IMAGES_PLAYER_1,
    ImageTag.IMAGES_PLAYER_1,
    ImageTag.IMAGES_PLAYER_1,
    ImageTag.IMAGES_PLAYER_1,
    ImageTag.IMAGES_PLAYER_0,
    ImageTag.IMAGES_PLAYER_0,
    ImageTag.IMAGES_PLAYER_0,
    ImageTag.IMAGES_PLAYER_0,
    ImageTag.IMAGES_PLAYER_1,
    ImageTag.IMAGES_PLAYER_1,
    ImageTag.IMAGES_PLAYER_1,
    ImageTag.IMAGES_PLAYER_1,
}


IMAGE_SURFACES : []^sdl2.Surface
IMAGE_TEXTURES : []^sdl2.Texture
MAP : [30 * 17]int
V2 :: [2]f32
Player :: struct{
    position: V2
    velocity: V2
    angle: f32
}
PLAYER : Player
Inputs :: struct{w, a, s, d: bool}
INPUT : Inputs
FRAME_INDEX := 0


Coin :: struct{
    x, y: int
    active: bool
    frame: int
}

COINS := [?]Coin{
    Coin{x=2, y=2, active=true, frame=1},
    Coin{x=3, y=2, active=true, frame=5},
    Coin{x=4, y=2, active=true, frame=9},
    Coin{x=5, y=2, active=true, frame=13},
    Coin{x=6, y=2, active=true, frame=17},
    Coin{x=7, y=2, active=true, frame=21},
    Coin{},
    Coin{},
    Coin{},
    Coin{},
}



frame :: proc(window: ^sdl2.Window, renderer: ^sdl2.Renderer) -> (running: bool) {
    FRAME_INDEX += 1
    event : sdl2.Event
    for sdl2.PollEvent(&event) {
        if event.type == sdl2.EventType.QUIT {
            return false
        }
        else if event.type == sdl2.EventType.KEYDOWN {
            #partial switch event.key.keysym.sym {
                case sdl2.Keycode.w:
                    INPUT.w = true
                case sdl2.Keycode.a:
                    INPUT.a = true
                case sdl2.Keycode.s:
                    INPUT.s = true
                case sdl2.Keycode.d:
                    INPUT.d = true
            }
        }
        else if event.type == sdl2.EventType.KEYUP {
            #partial switch event.key.keysym.sym {
                case sdl2.Keycode.w:
                    INPUT.w = false
                case sdl2.Keycode.a:
                    INPUT.a = false
                case sdl2.Keycode.s:
                    INPUT.s = false
                case sdl2.Keycode.d:
                    INPUT.d = false
            }
        }
    }
    sdl2.SetRenderDrawColor(renderer, 0, 0, 0, 255)
    sdl2.RenderClear(renderer)

    velocity: [2]f32
    if INPUT.w { velocity.y -= 1.0 }
    if INPUT.s { velocity.y += 1.0 }
    if INPUT.a { velocity.x -= 1.0 }
    if INPUT.d { velocity.x += 1.0 }

    PLAYER.velocity = linalg.normalize0(velocity) * 5.0
    PLAYER.position += PLAYER.velocity
    if linalg.length(PLAYER.velocity) > 0.001 {
        PLAYER.angle = linalg.degrees(linalg.atan2(PLAYER.velocity.y, PLAYER.velocity.x)) - 90
    }

    width := 30
    height := 17
    rect : sdl2.Rect
    rect.w = 64
    rect.h = 64

    for x := 0; x < width; x += 1 {
        rect.x = i32(x * 64)
        for y := 0; y < height; y += 1 {
            rect.y = i32(y * 64)
            index := y * width + x
            damage := MAP[index]
            switch damage {
            case 0:
                sdl2.RenderCopy(renderer, IMAGE_TEXTURES[ImageTag.IMAGES_FOUNDATION], nil, &rect)
                sdl2.RenderCopy(renderer, IMAGE_TEXTURES[ImageTag.IMAGES_FLOOR], nil, &rect)
            case 1:
                sdl2.RenderCopy(renderer, IMAGE_TEXTURES[ImageTag.IMAGES_LAVA], nil, &rect)
                sdl2.RenderCopy(renderer, IMAGE_TEXTURES[ImageTag.IMAGES_FLOOR], nil, &rect)
            case:
                sdl2.RenderCopy(renderer, IMAGE_TEXTURES[ImageTag.IMAGES_LAVA], nil, &rect)
            }
        }
    }

    rect.x = i32(PLAYER.position.x)
    rect.y = i32(PLAYER.position.y)

    point : sdl2.Point
    point.x = 32
    point.y = 32


    texture : ^sdl2.Texture
    if linalg.length(velocity) > 0.001 {
        texture = IMAGE_TEXTURES[RUNCYCLE[FRAME_INDEX % len(RUNCYCLE)]]
    }
    else {
        texture = IMAGE_TEXTURES[ImageTag.IMAGES_PLAYER_2]
    }

    sdl2.RenderCopyEx(
        renderer, texture, nil, &rect
        // Ex extras
        f64(PLAYER.angle), &point, sdl2.RendererFlip.NONE,
    )

    for i := 0; i < len(COINS); i += 1 {
        coin := COINS[i]
        if !coin.active {
            continue
        }

        coin_frame := FRAME_INDEX + coin.frame
        texture = IMAGE_TEXTURES[COINCYCLE[coin_frame % len(COINCYCLE)]]
        rect.x = i32(coin.x * 64)
        rect.y = i32(coin.y * 64)
        sdl2.RenderCopy(renderer, texture, nil, &rect)
    }

    sdl2.RenderPresent(renderer)
    return true
}


main :: proc() {

    if sdl2.Init(sdl2.INIT_EVERYTHING) != 0 {
        msg := sdl2.GetErrorString()
        fmt.println(msg)
        return
    }
    defer sdl2.Quit()

    if image.Init(image.INIT_PNG) != image.INIT_PNG {
        msg := sdl2.GetErrorString()
        fmt.println(msg)
        return
    }
    defer image.Quit()

    window := sdl2.CreateWindow(
        "Hail, Viking!",
        sdl2.WINDOWPOS_UNDEFINED, sdl2.WINDOWPOS_UNDEFINED,
        1920 / 2, 1080 / 2,
        sdl2.WINDOW_RESIZABLE | sdl2.WINDOW_SHOWN
    )
    if window == nil {
        msg := sdl2.GetErrorString()
        fmt.println(msg)
        return
    }
    defer sdl2.DestroyWindow(window)

    renderer := sdl2.CreateRenderer(window, -1, sdl2.RENDERER_ACCELERATED)
    if renderer == nil {
        msg := sdl2.GetErrorString()
        fmt.println(msg)
        return
    }
    defer sdl2.DestroyRenderer(renderer)


    IMAGE_SURFACES = make([]^sdl2.Surface, ImageTag.COUNT_IMAGES)
    IMAGE_TEXTURES = make([]^sdl2.Texture, ImageTag.COUNT_IMAGES)


    MAP[0] = 1
    MAP[1] = 2
    MAP[3] = 3
    MAP[4] = 1

    PLAYER.position.x = 432
    PLAYER.position.y = 230

    // Loading
    for tag in ImageTag {
        if tag == ImageTag.COUNT_IMAGES {
            break
        }
        // Load a surface
        surface := image.Load(IMAGE_PATHS[int(tag)])
        IMAGE_SURFACES[int(tag)] = surface

        // Upload to GPU
        texture := sdl2.CreateTextureFromSurface(renderer, surface)
        IMAGE_TEXTURES[int(tag)] = texture

    }


    running := true
    for running {
        running = frame(window, renderer)
        sdl2.Delay(16)
    }

}

