try:
    import cPickle as pickle
except ImportError:
    import pickle
import sys
from itertools import product
from random import randint


import pygame
from pygame.locals import *
from vector import Vector

WINDOW = pygame.display.set_mode((1920, 1080), pygame.FULLSCREEN)


def readScores():
    with open('highscores', 'rb') as f:
        scores = pickle.load(f)
    return scores


def recordScore(score, player):
    scores = readScores()
    scores.append((score, player))
    scores.sort(reverse=True)
    if len(scores) > 10:
        scores = scores[:10]
    writeScores(scores)


def writeScores(scores):
    with open('highscores', 'wb') as f:
        pickle.dump(scores, f, protocol=2)


class Coin:

    images = [pygame.image.load('images/coin_0.png').convert_alpha(),
              pygame.image.load('images/coin_1.png').convert_alpha(),
              pygame.image.load('images/coin_2.png').convert_alpha(),
              pygame.image.load('images/coin_3.png').convert_alpha(),
              pygame.image.load('images/coin_4.png').convert_alpha(),
              pygame.image.load('images/coin_5.png').convert_alpha()
              ]

    runcycle = [0, 0, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5]

    def __init__(self, x, y):
        self.cycle = 0
        self.x = x
        self.y = y

    @property
    def pos(self):
        return (self.x*64, self.y*64)

    @property
    def image(self):
        return self.images[self.runcycle[self.cycle]]

    def blit(self, surf):
        surf.blit(self.image, self.pos)
        self.cycle += 1
        self.cycle %= len(self.runcycle)


class Player:

    images = [pygame.image.load('images/player_0.png').convert_alpha(),
              pygame.image.load('images/player_1.png').convert_alpha(),
              pygame.image.load('images/player_2.png').convert_alpha(),
              pygame.image.load('images/player_3.png').convert_alpha(),
              pygame.image.load('images/player_4.png').convert_alpha()
              ]

    runcycle = [2, 2, 3, 3, 4, 4, 3, 3, 2, 2, 1, 1, 0, 0, 1, 1]

    def __init__(self, playerNumber=1, joystick=None):
        self.leapSound = pygame.mixer.Sound('sounds/bounce.wav')
        self.splashSound = pygame.mixer.Sound('sounds/splash.wav')
        self.scoreFont = pygame.font.Font('fonts/SF_Archery_Black_SC.ttf', 64)
        self.cycle = 0
        self.score = 0
        self.playerNumber = playerNumber

        if playerNumber == 1:
            x, y = 480, 270
        elif playerNumber == 2:
            x, y = 1440, 270
        elif playerNumber == 3:
            x, y = 480, 810
        elif playerNumber == 4:
            x, y = 1440, 810

        self.z = 0
        self.vz = 0
        self.pos = Vector(x, y)
        self.vel = Vector(0, -1)
        self.next = None
        self.dead = False

        if joystick:
            self.joystick = joystick
            self.move = self.moveJoy
        else:
            self.move = self.moveKey

    @property
    def x(self):
        return int((self.pos.x)/64)

    @property
    def y(self):
        return int((self.pos.y)/64)

    def die(self):
        if not self.dead:
            self.splashSound.play()
            self.dead = True

    def jump(self):
        if self.z == 0:
            self.leapSound.play()
            self.vz = 10

    def fall(self):
        self.vz -= 1
        self.z += self.vz

    def vroomVroom(self, speed):
        if self.z >= 0 and -self.vz > self.z:
            self.z = 0
            self.vz = 0
        else:
            self.z += self.vz
            self.vz -= 1
        if speed.length > 0:
            self.cycle += 1
            self.cycle %= len(self.runcycle)
            speed.set_length(10)
            self.pos += speed
            self.vel = speed
            # Keep da little guy on the screen
            if self.pos.x > 1910:
                self.pos.x = 1910
            elif self.pos.x < 10:
                self.pos.x = 10
            if self.pos.y > 1070:
                self.pos.y = 1070
            elif self.pos.y < 10:
                self.pos.y = 10
        else:
            self.cycle = 0

    def moveJoy(self):
        vx = self.joystick.get_axis(0)
        vy = self.joystick.get_axis(1)
        if self.joystick.get_button(0):
            self.jump()
        vel = Vector(vx, vy)
        if vel.length < 0.1:
            self.vroomVroom(Vector(0, 0))
        else:
            self.vroomVroom(vel)

    def moveKey(self):
        keys = pygame.key.get_pressed()
        v = Vector(0, 0)
        if keys[K_w] or keys[K_UP]:
            v.y -= 1
        if keys[K_s] or keys[K_DOWN]:
            v.y += 1
        if keys[K_a] or keys[K_LEFT]:
            v.x -= 1
        if keys[K_d] or keys[K_RIGHT]:
            v.x += 1
        if keys[K_SPACE]:
            self.jump()
        self.vroomVroom(v)

    def blit(self, surf):
        image = self.images[self.runcycle[self.cycle]].copy()
        if self.z != 0:
            size = int((64 + 1*self.z)/2)
            if size < 1:
                size = 1
            shadow = pygame.Surface((size*2, size*2)).convert_alpha()
            shadow.fill(pygame.color.Color(0, 0, 0, 0))
            pygame.draw.circle(shadow, pygame.color.Color(0, 0, 0, 127),
                               (size, size), int(size*1.5/2))
            image = pygame.transform.scale(image, (size*2, size*2))
            shadow.blit(image, (0, 0))
            image = shadow
        image = pygame.transform.rotate(image, 90-self.vel.angle)
        rect = image.get_rect()
        rect.center = (self.pos.x, self.pos.y)
        surf.blit(image, rect)
        score = self.scoreFont.render('P%d: %d' % (self.playerNumber,
                                                   self.score),
                                      True,
                                      pygame.Color(0, 255, 0))
        surf.blit(score, ((self.playerNumber-1)*480, 0))


def getTileImage(base, walkable, *bits):
    if base:
        surf = pygame.image.load('images/foundation.png').convert_alpha()
    else:
        surf = pygame.image.load('images/lava.png').convert_alpha()
    if walkable:
        surf2 = pygame.image.load('images/floor.png').convert_alpha()
        surf.blit(surf2, (0, 0))
    elif bits != (False, False, False, False):
        fn = 'images/pit'
        for bit in bits:
            if bit:
                fn += '1'
            else:
                fn += '0'
        fn += '.png'
        surf2 = pygame.image.load(fn).convert_alpha()
        surf.blit(surf2, (0, 0))
    return surf


def getTileImages():
    images = {}
    tf = (True, False)
    for comb in product(tf, tf, tf, tf, tf, tf):
        images[comb] = getTileImage(*comb)
    return images


class Tile:

    images = getTileImages()
    direcitons = {
        (0, -1): 0,
        (1, 0): 1,
        (0, 1): 2,
        (-1, 0): 3
    }

    def __init__(self, x, y, broadcast):
        self.damage = 0
        self.x = x
        self.y = y
        self.neighbors = [True, True, True, True]
        self.redraw = True
        self.broadcast = broadcast

    def receive(self, x, y, value):
        direction = (x-self.x, y-self.y)
        if direction in self.direcitons:
            direction = self.direcitons[direction]
            if self.neighbors[direction] != value:
                self.neighbors[direction] = value
                self.redraw = True

    @property
    def base(self):
        return self.damage == 0

    @property
    def walkable(self):
        return self.damage < 2

    @property
    def image(self):
        return self.images[self.base,
                           self.walkable,
                           self.neighbors[0],
                           self.neighbors[1],
                           self.neighbors[2],
                           self.neighbors[3]]

    @property
    def pos(self):
        return (self.x*64, self.y*64)

    def crumble(self):
        if self.walkable:
            self.damage += 1
            self.redraw = True
            if not self.walkable:
                self.broadcast(self.x, self.y, self.walkable)

    def blit(self, surf):
        if self.redraw:
            if self.image:
                surf.blit(self.image, self.pos)
                self.redraw = False


class Match:

    w = 30
    h = 17

    def __init__(self, surface):
        self.pickUpSound = pygame.mixer.Sound('sounds/pickup.wav')
        self.surface = surface
        self.coins = []
        self.players = []
        self.tiles = {}
        for i in range(self.w):
            for j in range(self.h):
                self.tiles[i, j] = Tile(i, j, self.broadcast)

    def broadcast(self, x, y, walkable):
        if (x, y-1) in self.tiles:
            self.tiles[x, y-1].receive(x, y, walkable)
        if (x+1, y) in self.tiles:
            self.tiles[x+1, y].receive(x, y, walkable)
        if (x, y+1) in self.tiles:
            self.tiles[x, y+1].receive(x, y, walkable)
        if (x-1, y) in self.tiles:
            self.tiles[x-1, y].receive(x, y, walkable)

    def randomTile(self):
        x = randint(0, self.w-1)
        y = randint(0, self.h-1)
        return self.tiles[x, y]

    def running(self):
        for p in self.players:
            if not p.dead:
                return True
        return False

    def update(self):
        for event in pygame.event.get():
            if event.type == USEREVENT+1:
                self.randomTile().crumble()
            elif event.type == USEREVENT+2:
                tile = self.randomTile()
                self.coins.append(Coin(tile.x, tile.y))
            elif event.type == USEREVENT+3:
                for player in self.players:
                    if player.next:
                        player.next.crumble()
                    player.next = self.tiles[player.x, player.y]
            elif event.type == USEREVENT+4:
                pygame.time.set_timer(USEREVENT+4, 0)
                pygame.time.set_timer(USEREVENT+1, 100)  # crumbling on
                pygame.time.set_timer(USEREVENT+2, 1000)  # coins on
                pygame.time.set_timer(USEREVENT+3, 250)  # player trail on

        for player in self.players:
            x, y = player.x, player.y
            if self.tiles[x, y].walkable or player.z > 0:
                player.move()
            elif player.z <= -50:
                player.die()
            elif player.z <= 0:
                player.fall()
            for tile in product((x-1, x, x+1), (y-1, y, y+1)):
                if tile in self.tiles:
                    self.tiles[tile].redraw = True

        for x in range(self.w):
            self.tiles[x, 0].redraw = True

        coins = []
        for coin in self.coins:
            if self.tiles[coin.x, coin.y].walkable:
                self.tiles[coin.x, coin.y].redraw = True
                pickedUp = False
                for player in self.players:
                    if player.x == coin.x and player.y == coin.y:
                        player.score += 1
                        self.pickUpSound.play()
                        pickedUp = True
                        break
                if not pickedUp:
                    coins.append(coin)
        self.coins = coins

    def render(self):
        for tile in self.tiles.values():
            tile.blit(self.surface)
        for coin in self.coins:
            coin.blit(self.surface)
        for player in self.players:
            player.blit(self.surface)


class Entry:

    keymap = {
        K_a: 'A',
        K_b: 'B',
        K_c: 'C',
        K_d: 'D',
        K_e: 'E',
        K_f: 'F',
        K_g: 'G',
        K_h: 'H',
        K_i: 'I',
        K_j: 'J',
        K_k: 'K',
        K_l: 'L',
        K_m: 'M',
        K_n: 'N',
        K_o: 'O',
        K_p: 'P',
        K_q: 'Q',
        K_r: 'R',
        K_s: 'S',
        K_t: 'T',
        K_u: 'U',
        K_v: 'V',
        K_w: 'W',
        K_x: 'X',
        K_y: 'Y',
        K_z: 'Z',
        K_0: '0',
        K_1: '1',
        K_2: '2',
        K_3: '3',
        K_4: '4',
        K_5: '5',
        K_6: '6',
        K_7: '7',
        K_8: '8',
        K_9: '9'
    }

    def __init__(self, title, field):
        self.title = title
        self.field = field
        self.blipSound = pygame.mixer.Sound('sounds/blip.wav')
        self.font = pygame.font.Font('fonts/SF_Archery_Black_SC.ttf', 32)
        self.surf = pygame.Surface((1000, 100))
        self.rect = self.surf.get_rect()

    def update(self):
        for event in pygame.event.get():
            if event.type == KEYDOWN:
                if event.key in (K_BACKSPACE, K_DELETE):
                    if len(self.field) > 0:
                        self.field = self.field[:-1]
                elif event.key == K_RETURN:
                    self.blipSound.play()
                    return self.field
                elif event.key in self.keymap:
                    if len(self.field) < 10:
                        self.field += self.keymap[event.key]
        return None

    def render(self):
        self.surf.fill((255, 255, 255))
        text = self.title + ': ' + self.field
        color = pygame.Color(0, 0, 0)
        image = self.font.render(text, True, color)
        r = image.get_rect()
        r.left, r.centery = self.rect.left, self.rect.centery
        self.surf.blit(image, r)
        return self.surf


class Menu:

    def __init__(self, title, options):
        self.blipSound = pygame.mixer.Sound('sounds/blip.wav')
        self.title = title
        self.options = options
        self.selection = 0
        self.lrg = pygame.font.Font('fonts/SF_Archery_Black_SC.ttf', 64)
        self.med = pygame.font.Font('fonts/SF_Archery_Black_SC.ttf', 48)
        self.sml = pygame.font.Font('fonts/SF_Archery_Black_SC.ttf', 32)
        self.black = pygame.Color(0, 0, 0)
        self.white = pygame.Color(255, 255, 255)
        self.green = pygame.Color(0, 255, 0)
        self.qs = self.lrg.render(self.title, True, self.black, self.white)
        self.qr = self.qs.get_rect()
        self.qr.left += 50
        h = 50
        h += self.lrg.get_linesize()
        h += self.med.get_linesize()*1.3
        h += self.sml.get_linesize()*(len(self.options)-1)*1.3
        w = self.qr.width+100
        self.surface = pygame.Surface((w, h))

    def next(self):
        self.blipSound.play()
        self.selection += 1
        self.selection %= len(self.options)

    def prev(self):
        self.blipSound.play()
        self.selection -= 1
        self.selection %= len(self.options)

    def submit(self):
        self.blipSound.play()
        return self.options[self.selection]

    def update(self):
        for event in pygame.event.get():
            if event.type == KEYDOWN:
                if event.key in (K_UP, K_w):
                    self.prev()
                elif event.key in (K_DOWN, K_s):
                    self.next()
                elif event.key == K_RETURN:
                    return self.submit()
            elif event.type == JOYBUTTONDOWN:
                if event.button == 0:
                    return self.submit()
            elif event.type == JOYHATMOTION:
                if event.value[1] == -1:
                    self.next()
                elif event.value[1] == 1:
                    self.prev()
        return None

    def render(self):
        self.surface.fill(self.white)
        self.surface.blit(self.qs, self.qr)
        top = self.lrg.get_linesize()
        for option in self.options:
            if option == self.options[self.selection]:
                f, c = self.med, self.green
            else:
                f, c = self.sml, self.black
            s = f.render(option, True, c, self.white)
            r = s.get_rect()
            r.centerx, r.top = self.surface.get_rect().centerx, top
            top += self.med.get_linesize()
            self.surface.blit(s, r)
        return self.surface


class Game:

    def __init__(self, window):
        self.window = window
        splash = pygame.image.load('images/splash.png').convert_alpha()
        self.window.blit(splash, (0, 0))
        self.rect = window.get_rect()
        self.clock = pygame.time.Clock()
        pygame.display.set_caption('The Floor is Lava')
        pygame.mouse.set_visible(False)
        self.joystickinit()
        self.call = self.runMenu
        while self.call():
            pass

    def joystickinit(self):
        pygame.joystick.quit()
        pygame.joystick.init()
        self.joysticks = []
        numJoysticks = pygame.joystick.get_count()
        for jn in range(numJoysticks):
            joystick = pygame.joystick.Joystick(jn)
            joystick.init()
            self.joysticks.append(joystick)

    def match1p(self):
        return self.runMatch(1)

    def match2p(self):
        return self.runMatch(2)

    def match3p(self):
        return self.runMatch(3)

    def match4p(self):
        return self.runMatch(4)

    def runMenu(self):
        menu = Menu('The Floor is Lava',
                    ['1-Player Game',
                     '2-Player Game',
                     '3-Player Game',
                     '4-Player Game',
                     'High Scores',
                     'Detect Gamepads',
                     'Exit Game'])
        restore = self.window.copy()
        while True:
            image = menu.render()
            r = image.get_rect()
            r.center = self.rect.center
            self.window.blit(image, r)
            answer = menu.update()
            if answer:
                if answer == '1-Player Game':
                    self.call = self.match1p
                    self.window.blit(restore, (0, 0))
                    return True
                elif answer == '2-Player Game':
                    self.call = self.match2p
                    self.window.blit(restore, (0, 0))
                    return True
                elif answer == '3-Player Game':
                    self.call = self.match3p
                    self.window.blit(restore, (0, 0))
                    return True
                elif answer == '4-Player Game':
                    self.call = self.match4p
                    self.window.blit(restore, (0, 0))
                    return True
                elif answer == 'High Scores':
                    self.showScores()
                elif answer == 'Detect Gamepads':
                    self.joystickinit()
                elif answer == 'Exit Game':
                    pygame.mouse.set_visible(True)
                    return False
            pygame.display.flip()
            self.clock.tick(30)

    def getPlayerName(self, playername):
        entry = Entry('Player Name for High Score', playername)
        restore = self.window.copy()
        while True:
            image = entry.render()
            r = image.get_rect()
            r.center = self.rect.center
            self.window.blit(image, r)
            answer = entry.update()
            if answer:
                self.window.blit(restore, (0, 0))
                return answer
            pygame.display.flip()
            self.clock.tick(30)

    def showScores(self):
        scores = readScores()
        scores = ['%d - %s' % (x[0], x[1]) for x in scores]
        menu = Menu('The Very Highest of Scores', scores)
        restore = self.window.copy()
        while True:
            image = menu.render()
            r = image.get_rect()
            r.center = self.rect.center
            self.window.blit(image, r)
            if menu.update():
                self.window.blit(restore, (0, 0))
                return None
            pygame.display.flip()
            self.clock.tick(30)

    def chooseControl(self, pn):
        options = ['KeyBoard']
        joys = ['Gamepad %d' % (i+1) for i, j in enumerate(self.joysticks)]
        options.extend(joys)
        if len(options) == 1:
            return Player(pn)
        menu = Menu('Choose Controls for P%d' % (pn), options)
        restore = self.window.copy()
        while True:
            image = menu.render()
            r = image.get_rect()
            r.center = self.rect.center
            self.window.blit(image, r)
            answer = menu.update()
            if answer:
                if answer == 'KeyBoard':
                    self.window.blit(restore, (0, 0))
                    return Player(pn)
                else:
                    for i, j in enumerate(self.joysticks):
                        if answer == 'Gamepad %d' % (i+1):
                            self.window.blit(restore, (0, 0))
                            return Player(pn, j)
            pygame.display.flip()
            self.clock.tick(30)

    def runMatch(self, numPlayers):
        m = Match(self.window)

        # Add players
        for pn in range(numPlayers):
            player = self.chooseControl(pn+1)
            m.players.append(player)

        #start game
        pygame.event.clear()
        pygame.time.set_timer(USEREVENT+4, 3000)
        while m.running():
            pygame.event.pump()
            m.update()
            m.render()
            pygame.display.flip()
            self.clock.tick(30)
        pygame.time.set_timer(USEREVENT+1, 0)  # crumbling off
        pygame.time.set_timer(USEREVENT+2, 0)  # coins off
        pygame.time.set_timer(USEREVENT+3, 0)  # player trail off

        # Record high score
        scores = [(p.score, 'P%d' % (p.playerNumber)) for p in m.players]
        scores.sort(reverse=True)
        for score in scores:
            highscores = readScores()
            if score[0] > highscores[-1][0]:
                name = self.getPlayerName(score[1])
                recordScore(score[0], name)

        self.call = self.runMenu
        return True

if __name__ == '__main__':
    pygame.mixer.pre_init(44100, -16, 1, 512)
    pygame.init()
    pygame.joystick.init()
    game = Game(WINDOW)
    pygame.quit()
    sys.exit()
